# This is a Client for Zeebe in Golang 
This is a microservice client for Zeebe, how send a name into the workflow

## Running it
You can run it. 
go run hello-world.go

Build excecute this command.

* For Linux 

```bash
<in_you_path>$ go build -o hello-world-rest-api hello-world.go 
```
* For Windows

```bash
<in_you_path>: go build -o hello-world-rest-api.exe hello-world.go
```

## Testing

For testing purposes, in the browser or Postman follow the URL

```bash
http://127.0.0.1:5001/hello-world-workflow/{put_your_name}
```

Like this example 

```bash
http://127.0.0.1:5001/hello-world-workflow/Leif
```

