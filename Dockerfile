FROM golang:1.8

WORKDIR /go/src/github.com/w00k/hello-world-rest-api
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["hello-world-rest-api"]