module github.com/w00k/go-api

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/zeebe-io/zeebe/clients/go v0.24.2
)
