package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/zeebe-io/zeebe/clients/go/pkg/zbc"
)

type server struct{}

/*
GetName : retorna el nombre que viene en el request
*/
func GetName(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	nombre := params["name"]

	log.Println(" *** name : ", nombre)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("{\"message\": \"Hello world " + nombre + "\"}"))
}

func PutWorkflow(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	name := params["name"]

	workflowMessage := CreateInstance(name)

	jsonWorkflowMessage, err := json.Marshal(workflowMessage)

	if err != nil {
		log.Printf("PutWorkflow.error to transform in json object")
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jsonWorkflowMessage))
}

const brokerAddr = "127.0.0.1:26500"

func DeployWorkflow() {
	var bpmnFile string = "hello-world-process.bpmn"
	log.Println("DeployWorkflow.start")
	log.Println("DeployWorkflow BPMN File ", bpmnFile)

	zbClient, err := zbc.NewClient(&zbc.ClientConfig{
		GatewayAddress:         brokerAddr,
		UsePlaintextConnection: true,
	})

	if err != nil {
		panic(err)
	}

	ctx := context.Background()
	response, err := zbClient.NewDeployWorkflowCommand().AddResourceFile(bpmnFile).Send(ctx)

	if err != nil {
		panic(err)
	}

	log.Println("DeployWorkflow.response : ", response.String())
	log.Println("DeployWorkflow.end")
}

func CreateInstance(name string) WorkflowMessage {
	log.Println("CreateInstance.start")

	var workflowMessage WorkflowMessage

	client, err := zbc.NewClient(&zbc.ClientConfig{
		GatewayAddress:         brokerAddr,
		UsePlaintextConnection: true,
	})

	if err != nil {
		panic(err)
	}

	// After the workflow is deployed.
	variables := make(map[string]interface{})
	variables["name"] = name

	request, err := client.NewCreateInstanceCommand().BPMNProcessId("Process_0cwblrf").LatestVersion().VariablesFromMap(variables)
	if err != nil {
		panic(err)
	}

	ctx := context.Background()
	msg, err := request.Send(ctx)
	if err != nil {
		panic(err)
	}

	workflowMessage = WorkflowMessage{
		StatusMessage:       "OK",
		WorkflowKey:         msg.GetWorkflowKey(),
		WorkflowInstanceKey: msg.GetWorkflowInstanceKey(),
	}

	log.Println("CreateInstance.variables sends : ", variables)
	log.Println(msg.String())
	log.Println("CreateInstance.end")

	return workflowMessage
}

type WorkflowMessage struct {
	StatusMessage       string `json:"status_message"`
	WorkflowKey         int64  `json:"workflow_key"`
	WorkflowInstanceKey int64  `json:"workflow_instance_key"`
}

func main() {
	DeployWorkflow()
	router := mux.NewRouter()
	log.Println("start")
	router.HandleFunc("/hello-world/{name}", GetName).Methods("GET")
	router.HandleFunc("/hello-world-workflow/{name}", PutWorkflow).Methods("GET")
	//En caso que el programa falle, baja el servicio
	log.Fatal(http.ListenAndServe(":5001", router))
}
